"""Tests for cogef/cogef2in1.py.
"""
from pathlib import Path
import pytest

from ase import Atoms
from ase.build import molecule
from ase.optimize import FIRE
from ase.calculators.emt import EMT
from ase.calculators.morse import MorsePotential
from ase.io import Trajectory

from cogef.cogef2d import COGEF2IN1
from cogef.utilities import insubdir
from cogef.test.au6harmonic import Au6harmonic


def test_cyclobutene(tmp_path):
    # Class COGEF2IN1
    fmax = 0.05

    pull_atoms = [9, 6]
    break_atoms = [2, 3]
    break_distance = 2.5
    stepsize = 0.25

    image = molecule('cyclobutene')
    image.calc = EMT()
    opt = FIRE(image)
    opt.run(fmax=fmax)

    images = [image]

    def initialize(image, curve_type, imagenum, new_opt, get_filename):
        """Initialize the image and return the trajectory name.
        """
        if curve_type == 'reactant':
            dir_pull = 'pull'
        elif curve_type == 'transition':
            dir_pull = 'pull_max'
        else:
            assert curve_type == 'product'
            dir_pull = 'pull_min'
        if get_filename:
            return tmp_path / dir_pull / ('cogef' + str(imagenum) + '.traj')
        image.calc = EMT()
        return image

    cogef = COGEF2IN1(pull_atoms, break_atoms, break_distance, images,
                      name=str(tmp_path / 'cogef2in1'),
                      optimizer=FIRE, fmax=fmax,
                      optimizer_logfile=None)
    # make sure that the name is properly set
    assert str(Path(cogef.name).name) == \
        'cogef2in1_{0}_{1}'.format(*pull_atoms)

    cogef.calc_all(stepsize, initialize,
                   reactant_trajectory='reactant.traj',
                   transition_trajectory1=(tmp_path / 'pull_max/cogef.traj'),
                   transition_trajectory2=(tmp_path / 'breakmax.traj'),
                   product_trajectory1=(tmp_path / 'pull_min/cogef.traj'),
                   product_trajectory2=(tmp_path / 'breakmin.traj'))
    assert len(Trajectory(insubdir('reactant.traj', cogef.name))) == 12
    assert len(Trajectory(tmp_path / 'pull_max/cogef.traj')) == 2
    assert len(Trajectory(tmp_path / 'breakmax.traj')) == 12
    assert len(Trajectory(tmp_path / 'pull_min/cogef.traj')) == 2
    assert len(Trajectory(tmp_path / 'breakmin.traj')) == 12


def H4_linear_Morse(fmax):
    """Create linear H4 optimzed with Morse"""
    image = Atoms('H4', positions=[(i, 0, 0) for i in range(4)])
    image.calc = MorsePotential()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def test_linear_H4(tmp_path):
    """Linear H4 as in test_cogef2d"""
    fmax = 0.05
    images = [H4_linear_Morse(fmax)]
    pull_atoms = [0, 3]
    break_atoms = [2, 3]
    break_distance = 3

    def initialize1d(image):
        image.calc = MorsePotential()
        return image

    cogef = COGEF2IN1(pull_atoms, break_atoms, break_distance, images,
                      name=str(tmp_path / 'cogef2in1'),
                      optimizer=FIRE, fmax=fmax,
                      optimizer_logfile=None)
    cogef.calc_reactant_curve(0.1, initialize1d)


def test_Au6_harmonic(tmp_path):
    """Au6 chain harmonic model with three atoms"""
    fmax = 0.001
    images = [Au6harmonic(fmax)]

    pull_atoms = [0, 2]
    break_atoms = [0, 1]
    break_distance = 4

    cogef = COGEF2IN1(pull_atoms, break_atoms, break_distance,
                      images,
                      name=str(tmp_path / 'cogef2in1'),
                      optimizer=FIRE, fmax=fmax,
                      optimizer_logfile=None)

    # pull until the bond is broken
    cogef.calc_reactant_curve(0.2)
    assert (cogef.images[-1].get_potential_energy()
            == pytest.approx(0, abs=1e-5))
    # cogef.calc_transition_curve(0.2)
