# Copyright (C) 2016-2019
# See accompanying license files for details.

"""Tests for cogef/cogef2d.py.
"""

from pathlib import Path
import pytest
import numpy as np

from ase import Atoms
from ase.optimize import FIRE
from ase.calculators.morse import MorsePotential
from ase.io import Trajectory
from ase.autoneb import AutoNEB

from cogef.generalized import COGEF1D
from cogef.cogef2d import FixedForce2D


def H4_linear_Morse_relaxed(fmax):
    """Create linear H4 optimzed with Morse"""
    image = Atoms('H4', positions=[(i, 0, 0) for i in range(4)])
    image.calc = MorsePotential()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def energies(trajname):
    traj = Trajectory(trajname)
    return np.array([atoms.get_potential_energy() for atoms in traj])


def create_linear_H4(cogef1d):
    """Create a new instance of 2D-linear H4"""
    def initialize(image):
        image.calc = MorsePotential()
        return image

    fmax = 0.05
    break_atoms = [1, 2]
    cogef2 = COGEF1D(*break_atoms, fmax=fmax,
                     optimizer_logfile=None, initialize=initialize)

    cogef3s = FixedForce2D(cogef1d, cogef2)

    return cogef3s, initialize


@pytest.fixture
def cogef_linear_H4(H4_cogef1d):
    return create_linear_H4(H4_cogef1d)


def test_linear_fixed_force(cogef_linear_H4):
    cogef2d, initialize = cogef_linear_H4

    # find maximum for an intermediate configuration

    i = 10
    stepsize = 0.05

    # find maximum
    cogef2d.find_barrier(stepsize, i)
    # default name
    breaktrajectory = (Path(cogef2d.cogef1d.name) / 'ff_{0}'.format(i)
                       / 'cogef1d_{0}_{1}'.format(1, 2)
                       / 'cogef.traj')
    enes = energies(breaktrajectory)
    assert len(enes) == 2
    assert enes.max() == pytest.approx(-3.523136325599446, 1e-4)

    # continue to find minimum
    cogef2d.find_minimum(stepsize, [i])
    enes = energies(breaktrajectory)
    assert len(enes) == 3
    assert enes.max() == pytest.approx(-3.523136325599446, 1e-4)
    assert enes.min() == pytest.approx(-4.702848925775359, 1e-4)

    indices = [8, 9]
    maxima = [cogef2d.find_barrier(stepsize, i)[0] for i in indices]
    assert (Path(cogef2d.cogef1d.name) / 'ff_8').is_dir()
    assert (Path(cogef2d.cogef1d.name) / 'ff_9').is_dir()

    assert len(maxima) == 2
    for i, j in enumerate(indices):
        assert (maxima[i].get_potential_energy(apply_constraint=False)
                >= cogef2d.cogef1d.images[j].get_potential_energy(
                    apply_constraint=False))


def test_barriers(cogef_linear_H4):
    cogef2d, initialize = cogef_linear_H4

    Eorg = cogef2d.cogef1d.get_energies()

    # find maximum
    stepsize = 0.01
    print('\nfind barriers in 5 steps max')
    indices = [2, 3, 4, 5]
    for i in indices:
        cogef2d.find_barrier(stepsize, i, maxsteps=5)

    forces, barriers = cogef2d.collect_barriers()
    assert len(forces) == 2
    assert Eorg == pytest.approx(cogef2d.cogef1d.get_energies())

    print('\nfind barriers in 15 steps max')
    for i in indices:
        cogef2d.find_barrier(stepsize, i, maxsteps=15)

    forces, barriers = cogef2d.collect_barriers()
    assert len(forces) == 3


def test_name(H4_cogef1d):
    """Ensure proper directory naming"""
    def initialize(image):
        image.calc = MorsePotential()
        return image

    fmax = 0.05
    break_atoms = [1, 2]
    name = 'other'
    cogef2 = COGEF1D(*break_atoms, fmax=fmax, name=name,
                     optimizer_logfile=None, initialize=initialize)

    cogef3s = FixedForce2D(H4_cogef1d, cogef2)
    i = 2
    cogef3s.find_barrier(0.01, i)
    assert (Path(cogef3s.cogef1d.name) / f'ff_{i}'
            / '{0}_{1}_{2}'.format(name, *break_atoms)
            / 'cogef.traj').is_file()


def test_neb(H4_cogef1d, cogef_linear_H4):
    cogef2d, initialize = cogef_linear_H4

    # find maximum
    i = 2
    stepsize = 0.05
    print('\nfind barrier')
    cogef2d.find_barrier(stepsize, i)

    barrier = cogef2d.neb_barrier(i)
    # value with stepsize=0.01 0.02798697188937993
    assert barrier == pytest.approx(0.026617877259068834)

    # make sure the neb trajectory is there
    fpath = Path(cogef2d.prop.name) / 'neb.traj'
    assert fpath.is_file()
    st_mtime = fpath.lstat().st_mtime

    cogef2d.neb_barrier(i)
    # second call should read previous result
    assert st_mtime == fpath.lstat().st_mtime

    # restart and get barrier
    cogef2d_b, initialize = create_linear_H4(H4_cogef1d)
    barrier_b = cogef2d.neb_barrier(i)
    assert barrier_b == pytest.approx(barrier)

    # add more images
    n = 2
    len0 = len(Trajectory(fpath))
    cogef2d.neb_refine(n=n, i=2)
    assert not fpath.is_file()
    len1 = len(Trajectory(Path(cogef2d.prop.name) / 'neb_restart.traj'))
    assert len0 + 2 * n == len1
    barrier = cogef2d.neb_barrier(i)
    assert barrier == pytest.approx(0.027996615384322965)

    autoneb_barrier = cogef2d.neb_barrier(i,
                                          nebcls=AutoNEB,
                                          nebkwargs={'additional': 5})
    # compare to value for stepsize=0.01
    assert autoneb_barrier == pytest.approx(0.02798697188937993, 1e-3)
