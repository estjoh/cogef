======================================
COnstraint GEometry to simulate Forces
======================================

COGEF is an ASE_ module which contains tools for simulating force-induced
bond-breaking reactions based on the COnstrained Geometries simulate External
Force (COGEF) method (Beyer, M. K. J. Chem. Phys. 2000, 112, 7307).

.. _ASE: https://wiki.fysik.dtu.dk/ase

This module can be used to answer the question: How much force is needed to
break a bond in a molecule? In a first step the maximum force can be obtained
from the maximum slope of the energy-extension curve when the end-to-end
distance of the molecule is stretched. In further steps, more precise rupture
forces in dependence of the loading rate can be
obtained by including the effects of temperature, a two-dimensional
investigation of the energy landscape, multiple transitions into broken
molecule forms and back reactions from the broken into the intact form.
Furthermore polymer effects and the spring effect of a cantilever can be
included to simulate force-extension curves from atomic force microscopy
experiments with constant pulling velocity.

Contents
========

.. toctree::

   install
   tutorials/tutorials
