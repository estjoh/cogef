import matplotlib.pyplot as plt
from cogef import COGEF1D


ncogef = COGEF1D(0, 2)
rcogef = COGEF1D(0, 2, name='rattle')

lw = 3

plt.subplot(211)
plt.plot(ncogef.get_distances(), ncogef.get_energies(), lw=lw,
         label='symmetric')
plt.plot(rcogef.get_distances(), rcogef.get_energies(), lw=lw,
         label='rattle')
plt.legend()
plt.ylabel(r'$U$ eV')


plt.subplot(212)
def RAgAu(cogef, label):
    r1 = [im.get_distance(0, 1) for im in cogef.images]
    p = plt.plot(cogef.get_distances(), r1, '-',
                 lw=lw, alpha=0.3, label=label + ' bond1')
    r2 = [im.get_distance(1, 2) for im in cogef.images]
    plt.plot(cogef.get_distances(), r2, '--', lw=lw, alpha=0.3,
             color=p[0].get_color(), label=label + ' bond2')
    

RAgAu(ncogef, 'symmetric')
RAgAu(rcogef, 'rattle')
plt.legend()
plt.ylabel(r'$R_{\rm AuAg}$')

plt.xlabel(r'$R_{\rm AgAg}$')

if 0:
    plt.show()
else:
    plt.savefig('symmetric_vs_rattle.png')
