.. module:: cogef1s

======================
AuAg\ :sub:`2`\  chain
======================

We want to simulate external forces by the application of
geometry constraints. This "COnstraint GEometry to simulate Forces"
is known as the COGEF method [Beyer2000]_.

1S-COGEF
========

The standard COGEF method constrains two atoms usually by their distance 
and relaxes all other degrees of freedom.
We want to perform this now for a linear Au-Ag-Ag chain
using the `ASE <https://wiki.fysik.dtu.dk/ase/gettingstarted/tut01_molecule/molecule.html#atoms>`__ built in `EMT <https://wiki.fysik.dtu.dk/ase/ase/calculators/emt.html?highlight=emt#ase.calculators.emt.EMT.>`__ potential.
Optimization/relaxation is done with `FIRE <https://wiki.fysik.dtu.dk/ase/ase/optimize.html?highlight=fire#ase.optimize.FIRE>`__ (Fast Inertial Relaxation Engine) 
The convergence critterion is that the force on all individual atoms should be less than `f_max <https://wiki.fysik.dtu.dk/ase/ase/optimize.html?highlight=fire#ase.optimize.FIRE>`__

We first relax the chain:

.. literalinclude:: AuAg2_1_relax.py

Now we pull on the ends (atom indices 0, 2) of the chain:

.. literalinclude:: AuAg2_2_1scogef.py

which creates the directory ``cogef_0_2`` and
writes the file ``cogef_0_2/cogef.traj``
containing the corresponding trajectory.
Observing the trajectory shows that eventually
the Ag-Ag bond breaks. The maximum
force needed for separation can be either obtained from a 
numerical derivative of the energy or directly from the forces:

.. literalinclude:: AuAg2_force.py

Dissociation
============

.. image:: 1scogef.png
	   
The figure shows the force-dependent probability distributions dp/dF for
different temperatures. 
The mean rupture force and standard deviation are defined by the peaks.


In reality, bonds can not withstand much smaller forces due to
temperature effects.
We may to include finite temperature into consideration.
This is done using
the ``Dissociation`` object that allows to obtain force-dependent ** link for the dissociation file
probability distributions  :math:`dp/dF` as shown in the figure above.

The figure was created with:

** does the code “AuAg2_3_dpdF.py” create the cogef1d.png?

.. literalinclude:: AuAg2_3_dpdF.py

The peaks define the average rupture force and the standard deviation.
These can be obtained using the ``Dissociation`` object:

.. literalinclude:: AuAg2_force_T.py

Rupture force calculation can be performed automatically, that is the number
of needed steps for molecule stretching is obtained. The loop in the next
example stops when there are enough images in order to obtain the
rupture force::

.. literalinclude:: AuAg2_rupture_force.py

Gibbs energies can be used instead of electronic energies by a vibrational
analysis and a calculation based on ``IdealGasThermo``. The difference
in rupture force is very small for this example:

.. literalinclude:: AuAg2_force_Gibbs.py


Most probable force
===================

Rupture force or the most probable force can also be obtained analytically
using Bell barrier or the General barrier. We can calculate the force dependent
peak values of rupture force, the distribution and the range of most probable
forces analytically.

.. literalinclude:: AuAg2_mpf.py

.. image:: mpf.png
