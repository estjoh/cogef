# creates: 1scogef.png

from ase import io
from ase.calculators.emt import EMT

from cogef import COGEF1D

image = io.read('AuAg2.traj')
image.calc = EMT()

fmax = 0.01
cogef = COGEF1D(0, 2, optimizer_logfile=None, fmax=fmax)

if not len(cogef.images):  # calculation was done already
    cogef.images = [image]

    stepsize = 0.1
    steps = 30
    cogef.move(stepsize, steps)
