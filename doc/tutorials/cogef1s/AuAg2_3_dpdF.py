# creates: 1scogef.png

import pylab as plt

from cogef import COGEF
from cogef import Dissociation
from cogef.units import nN

cogef = COGEF(0, 2)

alpha = 10  # [nN/s] The force is assumed to increase uniformly by this loading rate.
    
P = 101325.  # Pressure    [Pa]

# To have loading rate in nN/s and
# all forces in nN in the input and
# output of Dissociation methods
diss = Dissociation(cogef)


fstep = 0.003
for T in [5, 50, 100, 200, 300, 500]: # Temperature [K]
    # Automatic limits
    fmin, fmax = diss.get_force_limits(T, P, alpha, force_step=fstep,
                                       method='electronic')
    DPDF, F = diss.probability_density(
        T, P, loading_rate=alpha, force_max=fmax, force_min=fmin,
        force_step=fstep, method='electronic')
    p = plt.plot(F / nN, DPDF, 'o-', color=color,
                 label='T={0}K'.format(T))
    plt.axvline(x=cogef.get_maximum_force(method='use_energies') / nN,
                ls='--', color='k')

plt.legend(loc=2)
plt.ylim(0, 110)
plt.xlabel('F [nN]')
plt.ylabel('dp/dF [nN$^{-1}$]')
plt.savefig('1scogef.png')
