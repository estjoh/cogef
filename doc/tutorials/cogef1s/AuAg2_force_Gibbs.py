from ase.calculators.emt import EMT
from cogef import COGEF
from cogef import Dissociation
from cogef.units import nN

cogef = COGEF(0, 2)

LOADING_RATE = 10  # [nN/s]
T = 300      # Temperature [K]
P = 101325.  # Pressure    [Pa]


def initialize(image, dummy_remove):  # XXX
    image.calc = EMT()
    return image


fstep = 0.003
diss = Dissociation(cogef, initialize, vib_method='frederiksen')
diss.geometry = 'linear'  # Like in class IdealGasThermo

fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                   method='Gibbs')
force, error = diss.rupture_force_and_uncertainty(
    T, P, LOADING_RATE, fmax, fmin, fstep, method='Gibbs')
print('Rupture force (Gibbs): ({0:4.3f} +- '
      '{1:4.3f}) nN'.format(force / nN, error))
