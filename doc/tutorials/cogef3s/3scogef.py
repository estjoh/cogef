# Copyright (C) 2016-2019
# See accompanying license files for details.

import sys
import pylab as plt

from ase.atoms import Atoms
from ase import io
from ase.optimize import FIRE
from ase.calculators.morse import MorsePotential
from ase.units import J, m
from cogef import COGEF
from cogef.dissociation.diss_old import Dissociation

fmax = 0.05

image = Atoms('H10', positions=[(i, 0, 0) for i in range(10)])
image.calc = MorsePotential()
FIRE(image).run(fmax=fmax)


def initialize(image):
    image.set_calculator(MorsePotential())
    return image


cogef = COGEF(0, 8, optimizer=FIRE, optimizer_logfile=None, fmax=fmax)
stepsize = 0.03
steps = 35
if len(cogef.images) == 0:
    cogef.images = [image]
    cogef.pull(stepsize, steps, initialize)

# Get the 1S-COGEF path
energies, distances = cogef.get_energy_curve()
plt.figure(0)
plt.plot(distances, energies)
plt.xlabel(r'd [$\rm\AA$]')
plt.ylabel('U [eV]')
plt.savefig('1scogef_problem.png')

# Save the wrong rate rate constants
nN = J / m * 1e-9

T = 300      # Temperature [K]
P = 101325.  # Pressure    [Pa]
force_min = 1. * nN
force_max = 4. * nN
force_step = 0.1 * nN

#diss = Dissociation(cogef, force_unit='nN')
diss = Dissociation(cogef)
rates_1scogef, forces_1scogef = diss.get_rate_constants(
    T, P, force_max, force_min, force_step, method='electronic')
barriers_1scogef = []
for f_ext in forces_1scogef:
    barriers_1scogef.append(diss.electronic_energy_barrier(f_ext))

# 3S-COGEF ----------------------------------------

from cogef.cogef2d import COGEF2D, Dissociation2d
from os.path import join


def initialize2d_old_not_used(image, directory, imagenum, new_opt, get_filename):
    """Initialize the image or return the trajectory name.

    """
    if initialize(image, -1, new_opt, get_filename) == 'Finished':
        return 'Finished'
    if get_filename:
        return join(directory, 'cogef2d') + str(imagenum) + '.traj'


def initialize2d(image):
    image.set_calculator(MorsePotential())
    return image


cogef = COGEF2D([0, 8], [0, 1], optimizer=FIRE, fmax=fmax,
                fix_force=True)
cogef.images = io.Trajectory('cogef_0_8/cogef.traj')
# Set last image number where 1s-COGEF path configuration has an intact bond
cogef.set_last_intact_bond_image(26)

energy_tolerance = 0.01
diss = Dissociation2d(
    cogef, #force_unit='nN',
    transition_into_two_fragments=True,
    initialize=initialize2d)


def get_rates():
    # Use some global variables
    while True:
        try:
            rates = diss.get_rate_constants(
                T, P, force_max, force_min, force_step, method='electronic')
            break
        except ValueError:
            assert diss.error in [1, 2]
            # More images must be calculated
            if len(diss.needed_max_images) > 0:
                I = diss.needed_max_images[-1]
                diss.clean_needed_images_list()
                is_max = True
            elif len(diss.needed_min_images) > 0:
                I = diss.needed_min_images[-1]
                diss.clean_needed_images_list()
                is_max = False
            else:
                I = None
            if (I is None or len(cogef.images) <= I):
                # Calculate 1S-COGEF path further
                cogef.pull(stepsize, 1, initialize)
            else:
                if is_max:
                    cogef.calc_maximum_curve(
                        [I], stepsize,  # initialize2d=initialize2d,
                        max_trajectory='maximum.traj',
                        breakdirectory='pull',
                        min_trajectory='minimum.traj',
                        only_minimum_curve=not is_max,
                        energy_tolerance=energy_tolerance)
                else:
                    cogef.calc_maximum_curve(
                        [I], stepsize, initialize2d=initialize2d,
                        max_trajectory='maximum.traj',
                        breakdirectory='pull',
                        min_trajectory='minimum.traj',
                        and_minimum_curve=not is_max,
                        energy_tolerance=energy_tolerance)
    return rates


try:
    rates_3scogef, forces_3scogef = get_rates()
except ValueError as msg:
    print('ValueError: ')
    print(msg)

cogef.max_image_number = 30
rates_3scogef, forces_3scogef = get_rates()

barriers_3scogef = []
for f_ext in forces_3scogef:
    barriers_3scogef.append(diss.electronic_energy_barrier(f_ext))

plt.figure(1)
plt.plot(forces_1scogef, rates_1scogef, '--', color='blue', label='1S-COGEF',
         lw=2)
plt.plot(forces_3scogef, rates_3scogef, color='blue', label='3S-COGEF', lw=2)
plt.xlabel('F [nN]')
plt.ylabel('Rate constant k [1/s]', color='blue')
plt.yscale('log')
ax1 = plt.gca()
for tl in ax1.get_yticklabels():
    tl.set_color('blue')
plt.legend(loc=5)

ax2 = plt.twinx()
ax2.plot(forces_1scogef, barriers_1scogef, '--', color='green', lw=2)
ax2.plot(forces_3scogef, barriers_3scogef, color='green', lw=2)
try:
    ddagger = unichr(int('2021', 16))  # Python 2
except NameError:
    ddagger = chr(int('2021', 16))  # Python 3
plt.ylabel('Activation energy $\\Delta U^' + ddagger + '$ [eV]',
           color='green')
for tl in ax2.get_yticklabels():
    tl.set_color('green')

plt.xlim(1, 4)
plt.savefig('1scogef_error.png')

diss.save_rate_constants(
    T, P, force_max, force_min, force_step, method='electronic',
    fileout='bond1.dat')

if 1:
    # Remove files and directories
    from os import remove
    from shutil import rmtree
    for i in range(1, steps + 1):
        try:
            remove('cogef1s' + str(i) + '.traj')
        except OSError:
            pass
    for i in range(2, 15 + 1):
        try:
            rmtree('pull_' + str(i))
        except OSError:
            pass

cogef = COGEF2D([0, 8], [1, 2], 'cogef1s.traj', optimizer=FIRE, fmax=fmax,
                fix_force=True)
cogef.set_last_intact_bond_image(26)
cogef.max_image_number = 30

energy_tolerance = 0.01
diss = Dissociation2d(cogef, #force_unit='nN',
                      transition_into_two_fragments=True)
get_rates()
diss.save_rate_constants(
    T, P, force_max, force_min, force_step, method='electronic',
    fileout='bond2.dat')

if 1:
    # Remove files and directories
    from shutil import rmtree
    for i in range(2, 15 + 1):
        try:
            rmtree('pull_' + str(i))
        except OSError:
            pass

from numpy import array
from cogef import load_rate_constants, Minima, probability_density
from cogef import rupture_force_and_uncertainty_from_dpdf

loading_rate = 1000.    # nN/s as defined in the Dissociation2d objects
                        # used for saving rate constants and associated forces

rates_bond1, forces_bond1 = load_rate_constants('bond1.dat')
rates_bond2, forces_bond2 = load_rate_constants('bond2.dat')
forces = forces_bond1
assert forces == forces_bond2

minima = Minima()
minima.add_destination(rates_bond1)
minima.add_destination(rates_bond2)
dpdf = probability_density(minima, forces, loading_rate)
print('Final probabilities:')
print(minima.prob)
f_rup, f_err = rupture_force_and_uncertainty_from_dpdf(
    -array(dpdf[0]), forces)
print('Rupture force:')
print(str(f_rup) + 'nN')
print('Standard deviation:')
print(str(f_err) + 'nN')
